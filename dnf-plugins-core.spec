%global dnf_lowest_compatible 4.2.22
%global dnf_plugins_extra 2.0.0
%global hawkey_version 0.46.1

%bcond_without yumcompatibility
%bcond_without yumutils

Name:                  dnf-plugins-core
Version:               4.0.24
Release:               2
Summary:               Core Plugins for DNF
License:               GPLv2+
URL:                   https://github.com/rpm-software-management/dnf-plugins-core
Source0:               %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:             noarch
BuildRequires:         cmake gettext python3-sphinx
Requires:              python3-%{name} = %{version}-%{release}  
Requires:              dnf >= %{dnf_lowest_compatible} %{name} = %{version}-%{release}
Requires:              python3-dnf >= %{dnf_lowest_compatible}              
Provides:              dnf-command(builddep) dnf-command(changelog) dnf-command(config-manager)
Provides:              dnf-command(copr) dnf-command(debug-dump) dnf-command(debug-restore)
Provides:              dnf-command(debuginfo-install) dnf-command(download) dnf-command(repoclosure)
Provides:              dnf-command(repograph) dnf-command(repomanage) dnf-command(reposync)
Provides:              dnf-command(repodiff) dnf-plugins-extras-debug = %{version}-%{release}
Provides:              dnf-plugins-extras-repoclosure = %{version}-%{release}
Provides:              dnf-plugins-extras-repograph = %{version}-%{release}
Provides:              dnf-plugins-extras-repomanage = %{version}-%{release}
Provides:              dnf-plugin-builddep = %{version}-%{release}
Provides:              dnf-plugin-config-manager = %{version}-%{release}
Provides:              dnf-plugin-debuginfo-install = %{version}-%{release}
Provides:              dnf-plugin-download = %{version}-%{release}
Provides:              dnf-plugin-generate_completion_cache = %{version}-%{release}
Provides:              dnf-plugin-needs_restarting = %{version}-%{release}
Provides:              dnf-plugin-repoclosure = %{version}-%{release}
Provides:              dnf-plugin-repodiff = %{version}-%{release}
Provides:              dnf-plugin-repograph = %{version}-%{release}
Provides:              dnf-plugin-repomanage = %{version}-%{release}
Provides:              dnf-plugin-reposync = %{version}-%{release}
Provides:              yum-plugin-copr = %{version}-%{release}
Provides:              yum-plugin-changelog = %{version}-%{release}
Provides:              yum-plugin-auto-update-debug-info = %{version}-%{release}
Provides:              dnf-utils = %{version}-%{release}
Conflicts:             dnf-plugins-extras-common-data < %{dnf_plugins_extra}
Conflicts:             yum-utils < 1.1.31-520 yum-plugin-copr < 1.1.31-520
Obsoletes:             dnf-utils < %{version}-%{release}
Obsoletes:	       python2-dnf-plugin-migrate < %{version}-%{release}

%description
Core Plugins for DNF. This package enhances DNF with builddep, config-manager,
copr, debug, debuginfo-install, download, needs-restarting, repoclosure,
repograph, repomanage, reposync, changelog and repodiff commands. Additionally
provides generate_completion_cache passive plugin.

%package -n            python3-%{name}
Summary:               Core Plugins for DNF
%{?python_provide:%python_provide python3-%{name}}
BuildRequires:         python3-devel python3-dnf >= %{dnf_lowest_compatible} 
BuildRequires:         python3-dbus
Requires:              python3-distro python3-dnf >= %{dnf_lowest_compatible}
Requires:              python3-hawkey >= %{hawkey_version} python3-dateutil
Requires:              python3-dbus
Provides:              python3-dnf-plugins-extras-debug = %{version}-%{release}
Provides:              python3-dnf-plugins-extras-repoclosure = %{version}-%{release}
Provides:              python3-dnf-plugins-extras-repograph = %{version}-%{release}
Provides:              python3-dnf-plugins-extras-repomanage = %{version}-%{release}
Obsoletes:             python3-dnf-plugins-extras-debug < %{dnf_plugins_extra}
Obsoletes:             python3-dnf-plugins-extras-repoclosure < %{dnf_plugins_extra}
Obsoletes:             python3-dnf-plugins-extras-repograph < %{dnf_plugins_extra}
Obsoletes:             python3-dnf-plugins-extras-repomanage < %{dnf_plugins_extra}
Obsoletes:             python2-%{name} < %{version}-%{release}
Conflicts:             %{name} <= 0.1.5 python2-%{name} < %{version}-%{release}
Conflicts:             python-%{name} < %{version}-%{release}

%description -n        python3-%{name}
Core Plugins for DNF, Python 3 interface. This package enhances DNF with builddep,
config-manager, copr, debug, debuginfo-install, download, needs-restarting,
repoclosure, repograph, repomanage, reposync, changelog and repodiff commands.
Additionally provides generate_completion_cache passive plugin.

%package -n            python3-dnf-plugin-versionlock
Summary:               Version Lock Plugin for DNF
Requires:              python3-%{name} = %{version}-%{release}
Provides:              dnf-plugin-versionlock =  %{version}-%{release}
Provides:              python3-dnf-plugins-extras-versionlock = %{version}-%{release}
Provides:              dnf-command(versionlock)
Provides:              yum-plugin-versionlock = %{version}-%{release}
Provides:              dnf-plugins-extras-versionlock = %{version}-%{release}
Conflicts:             dnf-plugins-extras-common-data < %{dnf_plugins_extra}
Conflicts:             python2-dnf-plugin-versionlock < %{version}-%{release}
Obsoletes:             python3-dnf-plugins-extras-versionlock < %{dnf_plugins_extra}

%description -n python3-dnf-plugin-versionlock
Version lock plugin takes a set of name/versions for packages and excludes all other
versions of those packages. This allows you to e.g. protect packages from being
updated by newer versions.

%package -n            python3-dnf-plugin-post-transaction-actions
Summary:               Post transation actions Plugin for DNF
Requires:              python3-%{name} = %{version}-%{release}
Provides:              dnf-plugin-post-transaction-actions = %{version}-%{release}
Conflicts:             python2-dnf-plugin-post-transaction-actions < %{version}-%{release}

%description -n python3-dnf-plugin-post-transaction-actions
PosttransationactionsPlugin for DNF,Python 3 version.Plugin runs action(shell commands) after
transaction is completed.Actions are defined in action files.

%package -n             python3-dnf-plugin-leaves
Summary:                Leaves Plugin for DNF
Requires:               python3-%{name} = %{version}-%{release}
Provides:               python3-dnf-plugins-extras-leaves = %{version}-%{release} dnf-command(leaves)
Provides:               dnf-plugin-leaves = %{version}-%{release}
Provides:               dnf-plugins-extras-leaves = %{version}-%{release}
Conflicts:              dnf-plugins-extras-common-data < %{dnf_plugins_extra}
Conflicts:              python2-dnf-plugin-leaves < %{version}-%{release}
Obsoletes:              python3-dnf-plugins-extras-leaves < %{dnf_plugins_extra}

%description -n python3-dnf-plugin-leaves
Leaves Plugin for DNF, Python 3 version. List all installed packages not required by any other
installed package.

%package -n              python3-dnf-plugin-show-leaves
Summary:                 Show-leaves Plugin for DNF
Requires:                python3-%{name} = %{version}-%{release}
Requires:                python3-dnf-plugin-leaves = %{version}-%{release}
Provides:                dnf-plugin-show-leaves =  %{version}-%{release}
Provides:                python3-dnf-plugins-extras-show-leaves = %{version}-%{release}
Provides:                dnf-command(show-leaves) dnf-plugins-extras-show-leaves = %{version}-%{release}
Conflicts:               dnf-plugins-extras-common-data < %{dnf_plugins_extra}
Conflicts:               python2-dnf-plugin-show-leaves < %{version}-%{release}
Obsoletes:               python3-dnf-plugins-extras-show-leaves < %{dnf_plugins_extra}

%description -n python3-dnf-plugin-show-leaves
Show-leaves Plugin for DNF, Python 3 version. List all installed packages that are no longer
required by any other installed package after a transaction.

%package -n              python3-dnf-plugin-local
Summary:                 Local Plugin for DNF
Requires:                createrepo_c python3-%{name} = %{version}-%{release}
Provides:                dnf-plugin-local =  %{version}-%{release}
Provides:                python3-dnf-plugins-extras-local = %{version}-%{release}
Provides:                dnf-plugins-extras-local = %{version}-%{release}
Conflicts:               dnf-plugins-extras-common-data < %{dnf_plugins_extra}
Conflicts:               python2-dnf-plugin-local < %{version}-%{release}
Obsoletes:               python3-dnf-plugins-extras-local < %{dnf_plugins_extra}

%description -n python3-dnf-plugin-local
Local Plugin for DNF, Python 3 version. Automatically copy all downloaded packages to a repository
on the local filesystem and generating repo metadata.

%package_help

%prep
%autosetup -p1
mkdir build-py3

%build
pushd build-py3
  %cmake ../ -DPYTHON_DESIRED:FILEPATH=%{__python3} 
  %make_build
  make doc-man
popd

%install
pushd build-py3
  %make_install
popd

%find_lang %{name}
mv %{buildroot}%{_libexecdir}/dnf-utils-3 %{buildroot}%{_libexecdir}/dnf-utils
rm -vf %{buildroot}%{_libexecdir}/dnf-utils-*
mkdir -p %{buildroot}%{_bindir}
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/debuginfo-install
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/needs-restarting
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/find-repos-of-install
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/repo-graph
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/package-cleanup
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/repoclosure
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/repodiff
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/repomanage
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/repoquery
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/reposync
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/repotrack
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/yum-builddep
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/yum-config-manager
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/yum-debug-dump
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/yum-debug-restore
ln -sf %{_libexecdir}/dnf-utils %{buildroot}%{_bindir}/yumdownloader
ln -sf %{_mandir}/man1/dnf-utils.1.gz %{buildroot}%{_mandir}/man1/find-repos-of-install.1.gz
ln -sf %{_mandir}/man1/dnf-utils.1.gz %{buildroot}%{_mandir}/man1/repoquery.1.gz
ln -sf %{_mandir}/man1/dnf-utils.1.gz %{buildroot}%{_mandir}/man1/repotrack.1.gz

%check
pushd build-py3
ctest -VV
popd

%files
%{_libexecdir}/dnf-utils
%{_bindir}/debuginfo-install
%{_bindir}/needs-restarting
%{_bindir}/find-repos-of-install
%{_bindir}/package-cleanup
%{_bindir}/repo-graph
%{_bindir}/repoclosure
%{_bindir}/repodiff
%{_bindir}/repomanage
%{_bindir}/repoquery
%{_bindir}/reposync
%{_bindir}/repotrack
%{_bindir}/yum-builddep
%{_bindir}/yum-config-manager
%{_bindir}/yum-debug-dump
%{_bindir}/yum-debug-restore
%{_bindir}/yumdownloader

%files -n python3-%{name} -f %{name}.lang
%license COPYING
%doc AUTHORS README.rst
%ghost %attr(644,-,-) %{_var}/cache/dnf/packages.db
%config(noreplace) %{_sysconfdir}/dnf/plugins/copr.conf
%config(noreplace) %{_sysconfdir}/dnf/plugins/copr.d
%config(noreplace) %{_sysconfdir}/dnf/plugins/debuginfo-install.conf
%{python3_sitelib}/dnf-plugins/builddep.py
%{python3_sitelib}/dnf-plugins/changelog.py
%{python3_sitelib}/dnf-plugins/config_manager.py
%{python3_sitelib}/dnf-plugins/copr.py
%{python3_sitelib}/dnf-plugins/debug.py
%{python3_sitelib}/dnf-plugins/debuginfo-install.py
%{python3_sitelib}/dnf-plugins/download.py
%{python3_sitelib}/dnf-plugins/generate_completion_cache.py
%{python3_sitelib}/dnf-plugins/groups_manager.py
%{python3_sitelib}/dnf-plugins/needs_restarting.py
%{python3_sitelib}/dnf-plugins/repoclosure.py
%{python3_sitelib}/dnf-plugins/repodiff.py
%{python3_sitelib}/dnf-plugins/repograph.py
%{python3_sitelib}/dnf-plugins/repomanage.py
%{python3_sitelib}/dnf-plugins/reposync.py
%{python3_sitelib}/dnf-plugins/__pycache__/builddep.*
%{python3_sitelib}/dnf-plugins/__pycache__/changelog.*
%{python3_sitelib}/dnf-plugins/__pycache__/config_manager.*
%{python3_sitelib}/dnf-plugins/__pycache__/copr.*
%{python3_sitelib}/dnf-plugins/__pycache__/debug.*
%{python3_sitelib}/dnf-plugins/__pycache__/debuginfo-install.*
%{python3_sitelib}/dnf-plugins/__pycache__/download.*
%{python3_sitelib}/dnf-plugins/__pycache__/generate_completion_cache.*
%{python3_sitelib}/dnf-plugins/__pycache__/groups_manager.*
%{python3_sitelib}/dnf-plugins/__pycache__/needs_restarting.*
%{python3_sitelib}/dnf-plugins/__pycache__/repoclosure.*
%{python3_sitelib}/dnf-plugins/__pycache__/repodiff.*
%{python3_sitelib}/dnf-plugins/__pycache__/repograph.*
%{python3_sitelib}/dnf-plugins/__pycache__/repomanage.*
%{python3_sitelib}/dnf-plugins/__pycache__/reposync.*
%{python3_sitelib}/dnfpluginscore/

%files -n python3-dnf-plugin-versionlock
%config(noreplace) %{_sysconfdir}/dnf/plugins/versionlock.conf
%config(noreplace) %{_sysconfdir}/dnf/plugins/versionlock.list
%{python3_sitelib}/dnf-plugins/versionlock.*
%{python3_sitelib}/dnf-plugins/__pycache__/versionlock.*

%files -n python3-dnf-plugin-post-transaction-actions
%config(noreplace) %{_sysconfdir}/dnf/plugins/post-transaction-actions.conf
%{python3_sitelib}/dnf-plugins/post-transaction-actions.*
%{python3_sitelib}/dnf-plugins/__pycache__/post-transaction-actions.*

%files -n python3-dnf-plugin-leaves
%{python3_sitelib}/dnf-plugins/leaves.*
%{python3_sitelib}/dnf-plugins/__pycache__/leaves.*

%files -n python3-dnf-plugin-show-leaves
%{python3_sitelib}/dnf-plugins/show_leaves.*
%{python3_sitelib}/dnf-plugins/__pycache__/show_leaves.*

%files -n python3-dnf-plugin-local
%config(noreplace) %{_sysconfdir}/dnf/plugins/local.conf
%{python3_sitelib}/dnf-plugins/local.*
%{python3_sitelib}/dnf-plugins/__pycache__/local.*

%files help
%{_mandir}/man1/debuginfo-install.*
%{_mandir}/man1/needs-restarting.*
%{_mandir}/man1/repo-graph.*
%{_mandir}/man1/repoclosure.*
%{_mandir}/man1/repodiff.*
%{_mandir}/man1/repomanage.*
%{_mandir}/man1/reposync.*
%{_mandir}/man1/yum-builddep.*
%{_mandir}/man1/yum-config-manager.*
%{_mandir}/man1/yum-debug-dump.*
%{_mandir}/man1/yum-debug-restore.*
%{_mandir}/man1/yum-groups-manager.*
%{_mandir}/man1/yumdownloader.*
%{_mandir}/man1/package-cleanup.*
%{_mandir}/man1/dnf-utils.*
%{_mandir}/man1/yum-utils.*
%{_mandir}/man1/find-repos-of-install.*
%{_mandir}/man1/repoquery.*
%{_mandir}/man1/repotrack.*
%{_mandir}/man8/yum-versionlock.*
%{_mandir}/man5/yum-versionlock.*
%{_mandir}/man8/dnf-versionlock.*
%{_mandir}/man8/dnf-builddep.*
%{_mandir}/man8/dnf-changelog.*
%{_mandir}/man8/dnf-config-manager.*
%{_mandir}/man8/dnf-copr.*
%{_mandir}/man8/dnf-debug.*
%{_mandir}/man8/dnf-debuginfo-install.*
%{_mandir}/man8/dnf-download.*
%{_mandir}/man8/dnf-generate_completion_cache.*
%{_mandir}/man8/dnf-groups-manager.*
%{_mandir}/man8/dnf-needs-restarting.*
%{_mandir}/man8/dnf-repoclosure.*
%{_mandir}/man8/dnf-repodiff.*
%{_mandir}/man8/dnf-repograph.*
%{_mandir}/man8/dnf-repomanage.*
%{_mandir}/man8/dnf-reposync.*
%{_mandir}/man1/yum-changelog.*
%{_mandir}/man8/yum-copr.*
%{_mandir}/man8/dnf-post-transaction-actions.*
%{_mandir}/man8/dnf-leaves.*
%{_mandir}/man8/dnf-show-leaves.*
%{_mandir}/man8/dnf-local.*

%changelog
* Fri May 6 2022 wuchaochao <cyanrose@yeah.net> - 4.0.24-2
- reomove BuildRequires python-nose

* Tue Nov 23 2021 hanhui<hanhui15@huawei.com> - 4.0.24-1
- DESC:update to 4.0.24

* Tue Jul 20 2021 liuyumeng <liuyumeng5@huawei.com> - 4.0.17-3
- delete gdb in buildrequires

* Fri Apr 16 2021 hanhui <hanhui15@huawei.com> - 4.0.17-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:return error when dnf download failed

* Tue Apr 28 2020 zhouyihang <zhouyihang3@huawei.com> - 4.0.17-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update dnf-plugins-core version to 4.0.17

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 4.0.11-5
- bugfix about update

* Fri Feb 28 2020 zhangrui <zhangrui182@huawei.com> -4.0.11-4
- fix fail in update

* Tue Feb 18 2020 zhangrui <zhangrui182@huawei.com> -4.0.11-3
- remove python2

* Wed Jan 8 2020 zhangrui <zhangrui182@huawei.com> - 4.0.11-2
- Package init.
